import tcod as lc
import os

def main():
    screen_width = 80
    screen_height = 50
    
    file_path = os.path.dirname(os.path.abspath(__file__))
    os.chdir(file_path)
    
    lc.console_set_custom_font("arial10x10.png", lc.FONT_TYPE_GREYSCALE | lc.FONT_LAYOUT_TCOD)
    lc.console_init_root(screen_width, screen_height, "Learning libtcod")
    
    while not lc.console_is_window_closed():
        lc.console_set_default_foreground(0, lc.white)
        lc.console_put_char(0, 15, 15, "@", lc.BKGND_NONE)
        lc.console_flush()
        
        key = lc.console_check_for_keypress()
        if key.vk == lc.KEY_ESCAPE:
            return True

if __name__ == "__main__":
    main()